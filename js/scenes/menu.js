import PlayScene from "./playscene.js";

export default class Menu extends Phaser.Scene {

    constructor() {
        super({ key: 'Menu', active: false });
        this.gameOver = false;

    }

    preload() {
        this.load.image('background', './img/background.png');
        this.load.image('ship', './img/ship.png');
        this.load.image('titulo', './img/titulo.png');
        this.load.audio('musica', './Music/1.ogg');
    }

    create() {
        this.add.image(0, 0, 'background');
        this.add.image(640, 0, 'background');
        this.add.image(0, 480, 'background');
        this.add.image(640, 480, 'background');
        this.add.image(400,300,'titulo', './img/titulo.png',{size:'200px'});

        this.musica = this.sound.add('musica');
        this.musica.play({loop: true});
        
        this.add.text(200,500,'Press SpaceBar to Start',{"fontSize": '25px'});
        //this.add.image(200, 200, 'ship');
        this.ship = this.physics.add.image(100, 200, 'ship');
        this.cursors = this.input.keyboard.createCursorKeys();
    }

    update() {
        if(this.cursors.space.isDown){
           // this.physics.velocityFromRotation(this.ship.rotation, 300, this.ship.body.acceleration);
                this.scene.stop();
                this.musica.stop();
                this.scene.start("PlayScene");
        }
    }

}   