import PlayScene from "./playscene.js";
//import Menu from "./menu.js";

export default class GameOverScene extends Phaser.Scene {
    constructor() {
        super({ key: 'GameOverScene', active: false });
        this.gameOver = false;

    }

    preload() {
        this.load.image('background', './img/background.png');
        this.load.image('ship', './img/ship.png');
        this.load.audio('musica-over', './Music/4.ogg')
    }

    create(data) {
        this.add.image(0, 0, 'background');
        this.add.image(640, 0, 'background');
        this.add.image(0, 480, 'background');
        this.add.image(640, 480, 'background');
        this.musica =  this.sound.add('musica-over');
        this.musica.play();

        this.add.text(200,200,'SCORE ' + data.score,{"fontSize": '30px'});
        this.add.text(400,300,'GAME OVER',{"fontSize": '50px'});
        this.add.text(200,500,'Press SHIFT to Restart or SPACEBAR to Menu',{"fontSize": '25px'});
        //this.add.image(200, 200, 'ship');
        this.ship = this.physics.add.image(100, 200, 'ship');
        this.cursors = this.input.keyboard.createCursorKeys();
    }

    update() {
        if(this.cursors.space.isDown){
            // this.physics.velocityFromRotation(this.ship.rotation, 300, this.ship.body.acceleration);
            this.musica.stop();
            this.scene.start("Menu");
        } else if (this.cursors.shift.isDown){
            this.musica.stop();
            this.scene.start("PlayScene");
        }

    }

}