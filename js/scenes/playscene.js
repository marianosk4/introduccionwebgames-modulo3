import GameOverScene from './gameoverscene.js';
import Shoot from './../gameObjects/shoot.js';
import Asteroid from '../gameObjects/asteroid.js';

export default class PlayScene extends Phaser.Scene {

    constructor() {
        super({key: 'PlayScene', active: false});
        this.lastFired = 0;
        this.asteroidElapsedTime = 4000; //asteroide cada 4000ms
        this.gameOver = false;

    }


    preload() {
        this.load.image('background', './img/background.png');
        this.load.image('ship', './img/ship.png');
        this.load.image('asteroid-1', './img/asteroid-1.png');
        this.load.image('asteroid-3', './img/asteroid-3.png');
        this.load.image('asteroid-5', './img/asteroid-5.png');
        this.load.image('shoot', './img/shoot.png');
        this.load.audio('asteroide', './Sound/2.wav');
        this.load.audio('disparo', './Sound/awp1.wav');
    }

    create() {

        this.add.image(0, 0, 'background');
        this.add.image(640, 0, 'background');
        this.add.image(0, 480, 'background');
        this.add.image(640, 480, 'background');

        this.ship = this.physics.add.image(400, 300, 'ship');
        this.ship.setDamping(true);
        this.ship.setDrag(0.99);
        this.ship.setMaxVelocity(200);
        this.ship.setCollideWorldBounds(true);
        this.ship.setSize(20, 30);

        this.sonidoAsteroide = this.sound.add('asteroide');
        this.sonidoDisparo = this.sound.add('disparo');

        //contador de vidas
        this.vidas = 3;

        //array de imagenes para mostrar cuantas vidas quedan
        this.ArrayVidas = [this.add.sprite(20, 20, 'ship'), this.add.sprite(60, 20, 'ship'), this.add.sprite(100, 20, 'ship')];

        this.score = 0;
        this.scoreText = this.add.text(600, 16, "score: 0", {fontSize: '32px', fill: '#FFF'});


        this.cursors = this.input.keyboard.createCursorKeys();

        this.shootsGroup = this.physics.add.group({
            classType: Shoot,
            maxSize: 10,
            runChildUpdate: true
        });

        this.asteroidsGroup = this.physics.add.group();
        this.asteroidsArray = [];

        //creo los mini asteroides aparte
        //si lo hacemos dentro del grupo de asteroides grandes,
        // se crea un loop en el que se siguen generando
        // miniasteroides cada vez que destruimos un miniasteroide
        this.miniAsteroidsGroup = this.physics.add.group();
        this.miniAsteroidsArray = [];

        this.asteroidsTimedEvent = this.time.addEvent({
            delay: this.asteroidElapsedTime,
            callback: this.addAsteroid,
            callbackScope: this,
            loop: true
        });

        //evento aparte para alternar los asteroides que vienen desde abajo
        this.asteroidsDownTimedEvent = this.time.addEvent({
            delay: this.asteroidElapsedTime + 1000,
            callback: this.addAsteroidDown,
            callbackScope: this,
            loop: true
        });

        this.physics.add.overlap(this.ship, this.asteroidsGroup, this.hitShip, null, this);
        this.physics.add.overlap(this.ship, this.miniAsteroidsGroup, this.hitShip, null, this);
        this.physics.add.collider(this.shootsGroup, this.asteroidsGroup, this.hitShoot, null, this);
        this.physics.add.collider(this.shootsGroup, this.miniAsteroidsGroup, this.hitShootMini, null, this);
    }

    update(time, delta) {

        if (this.gameOver) {
            this.scene.restart();
            this.gameOver = false;
            //this.scene.stop("Menu");
            this.scene.start("GameOverScene", {score: this.score});
        }

        if (this.cursors.up.isDown) {
            this.physics.velocityFromRotation(this.ship.rotation, 200, this.ship.body.acceleration);
        } else {
            this.ship.setAcceleration(0);
        }

        if (this.cursors.space.isDown && time > this.lastFired) {
            let shoot = this.shootsGroup.get();
            this.sonidoDisparo.play();

            if (shoot) {
                shoot.fire(this.ship.x, this.ship.y, this.ship.rotation);

                this.lastFired = time + 50;
            }
        }

        if (this.cursors.left.isDown) {
            this.ship.setAngularVelocity(-300);
        } else if (this.cursors.right.isDown) {
            this.ship.setAngularVelocity(300);
        } else {
            this.ship.setAngularVelocity(0);
        }

        this.asteroidsArray = this.asteroidsArray.filter(function (asteroid) {
            return asteroid.active;
        });

        this.miniAsteroidsArray = this.miniAsteroidsArray.filter(function (miniAsteroid) {
            return miniAsteroid.active;
        });

        for (let asteroid of this.asteroidsArray) {

            if (!asteroid.isOrbiting()) {
                asteroid.fire(this.ship.x, this.ship.y);
            }

            asteroid.update(time, delta);
        }


        for (let miniAsteroid of this.miniAsteroidsArray) {
            if (!miniAsteroid.isOrbiting()) {
                miniAsteroid.fireMini(this.ship.x,this.ship.y);
            }
            miniAsteroid.update(time,delta);
        }


    }

    addAsteroid() {
        let asteroid = new Asteroid(this, 200, 300, 'asteroid-1', 0);
        let quickAsteroid = new Asteroid(this, 200, 300, 'asteroid-3', 0);
        quickAsteroid.setSpeed(200);
        this.asteroidsGroup.add(asteroid, true);
        this.asteroidsArray.push(asteroid);
        this.asteroidsGroup.add(quickAsteroid, true);
        this.asteroidsArray.push(quickAsteroid);
    }

    addAsteroidDown() {
        let asteroidDown = new Asteroid(this, 200, 300, 'asteroid-1', 300);
        let quickAsteroidDown = new Asteroid(this, 200, 300, 'asteroid-3', 0);
        asteroidDown.setYOrigin(600);
        quickAsteroidDown.setYOrigin(600);
        quickAsteroidDown.setSpeed(200);
        this.asteroidsGroup.add(asteroidDown, true);
        this.asteroidsGroup.add(quickAsteroidDown, true);
        this.asteroidsArray.push(asteroidDown);
        this.asteroidsArray.push(quickAsteroidDown);
    }

    //creo 3 asteroides, los cargo en el array de mini
    addMiniAsteroid(asteroid) {
        for (let i = 1; i <= 3; i++) {
            let miniAsteroid = new Asteroid(this, 200, 300, 'asteroid-5', 0);
            miniAsteroid.setYOrigin(asteroid.y+(i*10));
            miniAsteroid.setxOrigin(asteroid.x+(i*10));
            miniAsteroid.setDirection(asteroid.direction+(i*5));

            this.miniAsteroidsGroup.add(miniAsteroid,true);
            this.miniAsteroidsArray.push(miniAsteroid);
        }
    }


    hitShip(ship, asteroid) {
        if (this.vidas === 1) {
            this.gameOver = true;
        } else {
            if (this.vidas > 2) {
                this.ArrayVidas[2].destroy();
            } else {
                this.ArrayVidas[1].destroy();
            }
            this.vidas--;
            ship.body.reset(400, 300);
            asteroid.disableBody(true, true);
        }
    }

    hitShoot(shoot, asteroid) {
        this.score += 10;
        this.scoreText.setText("Score: " + this.score);
        asteroid.disableBody(true, true);
        this.sonidoAsteroide.play();
        this.addMiniAsteroid(asteroid);
    }

    //comportamiento parecido a hitShoot,
    // pero se destruyen definitivamente en vez de crear uno nuevo
    hitShootMini(shoot, miniAsteroid) {
        this.score += 10;
        this.scoreText.setText("Score: " + this.score);
        miniAsteroid.disableBody(true, true);
        this.sonidoAsteroide.play();
    }
}