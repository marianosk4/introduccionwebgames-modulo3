export default class Asteroid extends Phaser.Physics.Arcade.Sprite {

    constructor (scene, x, y,texture) {
        super(scene, x, y, texture);
        this.speed = Phaser.Math.GetSpeed(100, 1);
        this.direction = 0;
        this.angle = 0;
        this.orbiting = false;
        this.direction = 0;
        this.factor = 1;
        this.yOrigin = 0; //pase yOrigin aca para poder setearlo desde setYOrigin
        this.xOrigin = Phaser.Math.RND.between(0, 800);
    }

    isOrbiting(){
        return this.orbiting;
    }

    fire(shipX, shipY) {
        this.setSize(32, 29);
        this.orbiting = true;

        this.setActive(true);
        this.setVisible(true);

        this.setPosition(this.xOrigin, this.yOrigin);

        if (shipX > this.xOrigin){
            let m = (shipY - this.yOrigin) / (shipX - this.xOrigin);
            this.direction = Math.atan(m);
        } else {
            this.factor = -1;
            let m =  (shipY - this.yOrigin) / (this.xOrigin - shipX);
            this.direction = Math.atan(m);
        }

        this.angleRotation = Phaser.Math.RND.between(0.2, 0.9);



    }

    //crear un asteroide sin direccion para setearsela despues
    fireMini(){
        this.setSize(17, 15);
        this.orbiting = true;

        this.setVisible(true);

        this.setPosition(this.xOrigin,this.yOrigin);

        this.angleRotation = Phaser.Math.RND.between(0.2, 0.9);
    }

    update(time, delta) {
        this.x += this.factor * Math.cos(this.direction) * this.speed * delta;
        this.y += Math.sin(this.direction) * this.speed * delta;
        this.angle += this.angleRotation;
        
        if (this.x < -50 || this.y < -50 || this.x > 800 || this.y > 600){
            this.setActive(false);
            this.setVisible(false);
        }
    }



    //setea el origen en el eje y
    setYOrigin(y){
        this.yOrigin=y;
    }

    //setear el origen en el eje x (se usa para crear los mini asteroides)
    setxOrigin(x){
        this.xOrigin=x;
    }

    //setea la velocidad del asteroide
    setSpeed(sp){
        this.speed = Phaser.Math.GetSpeed(sp, 1);
    }

    //setea la direccion del asteroide
    setDirection(dir){
        this.direction=dir;
    }
}
