import Menu from './scenes/menu.js';
import PlayScene from './scenes/playscene.js';
import GameOverScene from "./scenes/gameoverscene.js";

const config = {
    width: 800,
    height: 600,
    type: Phaser.AUTO,
    physics:{
        default: "arcade",
        arcade: {
            fps: 60,
            gravity: { y: 0 }
        }
    },
    scene: [Menu, PlayScene, GameOverScene]
}

const game = new Phaser.Game(config);